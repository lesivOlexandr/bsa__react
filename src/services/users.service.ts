import { dataStorageProvider } from '../storage/firebase';
import { User } from '../types/User';
import { Observable } from '../types/Observable';

export class UserService extends Observable<User[]> {
  protected onFirstSubscription(): void {
    // it's still unsafe because anybody can get user password by this way
    // but I guess for demonstrating purposes it's ok
    dataStorageProvider.collection('users').onSnapshot(docs => {
      const users: User[] = [];
      docs.forEach(doc => users.push(doc.data() as User));
      this.setUsers(users);
    })
  }

  private setUsers(users: User[]) {
    this.notifyObservers(users);
  }
}

export const userService = new UserService();