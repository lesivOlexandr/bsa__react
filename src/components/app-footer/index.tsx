import React from 'react';

import './index.css'

export const AppFooter = () => {
  return (
    <div className="app-footer">
      <div className="app-footer__copyright">
        Copyright
      </div>
    </div>
  );
}