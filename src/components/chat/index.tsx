import React from 'react';
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import { MessageList } from '../message-list';
import { ChatHeader } from '../chat-header';
import { MessageInput } from '../message-input';

export const ChatComponent = (
  {messages, user, users}:
  {messages: Message[], user: User, users: User[]}
) => {

  return (
    <div className="chat">
      <ChatHeader messages={messages} users={users} />
      <MessageList messages={messages} user={user} />
      <MessageInput user={user}/>
    </div>
  )
}

(ChatComponent as any).propTypes = {
  messages: propTypes.arrayOf(propTypes.object).isRequired,
  users: propTypes.arrayOf(propTypes.object).isRequired,
  user: propTypes.object.isRequired
}
