import React, { useState, FormEvent, ChangeEvent } from 'react';
import classNames from "classnames";
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import './index.css';
import { formatDate } from '../../helpers/date-helpers';
import { messageService } from '../../services/message.service';

export const MessageComponent = (
  {message, user}:
  {message: Message, user: User}
) => {
  const [isEditing, setEditing] = useState<boolean>(false);
  const [isLiked, setIsLiked] = useState<boolean>(message.likedBy.includes(user.id));
  const isMessageOfCurrentUser: boolean = message.user === user.name;

  let inputtedEditText: string | null = null;

  const onClickLike = () => {
    if (isMessageOfCurrentUser) {
      alert('You can\'t like your own post');
      return;
    }
    if (isLiked) {
      setIsLiked(false);
      message.likedBy = message.likedBy.filter((userId: string) => user.id !== userId);
    }
    if (!isLiked) {
      setIsLiked(true);
      message.likedBy.push(user.id)
    }
    messageService.updateMessage(message.id, message);
  }

  const onClickDelete = () => {
    messageService.deleteMessage(message.id);
  }

  const onEditMessageChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    inputtedEditText = e.target.value;
  }

  const onEditSubmit = (e: FormEvent) => {
    e.preventDefault();
    setEditing(false);
    if (inputtedEditText) {
      message.text = inputtedEditText;
      message.editedAt = Date.now();
      messageService.updateMessage(message.id, message);
    }
  }

  const onEditCancel = () => {
    setEditing(false);
    inputtedEditText = null;
  }

  return ( !isEditing ?
    <div className={classNames({
      'chat-body__chat-message': true,
      'chat-body__chat-message_user-current': isMessageOfCurrentUser
    })}>
      {
        !isMessageOfCurrentUser &&
        <div className="chat-message__user-avatar-wrapper">
          <img className="chat-message__user-avatar" src={message.avatar ? message.avatar: '/user.png'} alt="" />
        </div>
      }
      <div className="chat-message__text-wrapper">
        <p className="chat-message__text">
          { message.text }
        </p>
        <div className="chat-message__message-meta">
          <div className="message-meta__message-icons">
            <div className={classNames({
              "message-icons__likes-block": true,
              "message-meta__like-icon_owner-current-user": isMessageOfCurrentUser,
              "message-meta__like-icon_state-active": isLiked
            })}>
              <span className="likes-block__likes-count">
                {message.likedBy.length}
              </span>
              <span 
                onClick={onClickLike}
                className={classNames({
                  "material-icons message-meta__like-icon": true,
                  "message-icon": true
                })}>
                thumb_up
              </span>
            </div>
            {isMessageOfCurrentUser &&
              <>
                <span 
                  className="material-icons message-icon"
                  onClick={() => setEditing(true)}
                >
                  create
                </span>
                <span 
                  className="material-icons message-icon"
                  onClick={onClickDelete}
                >
                  delete
                </span>
              </>
            }
          </div>
          <div className="message-meta__time-send">
            { message.editedAt 
              ? 'Edited at ' + formatDate(new Date(message.editedAt))
              : 'Created at ' + formatDate(new Date(message.createdAt)) }
          </div>
        </div>
      </div>
    </div>
    // if isEditing
    : 
    <div className={classNames({
      'chat-body__chat-message': true,
      'chat-body__chat-message_user-current': isMessageOfCurrentUser
    })}>
      <form className="chat-message__edit-wrapper" onSubmit={onEditSubmit} onReset={onEditCancel}>
        <textarea className="chat-message__edit" defaultValue={message.text} onChange={onEditMessageChange} />
        <div className="chat-message__buttons">
          <button className="chat-message__submit-edit-button" type="submit">Save</button>
          <button className="chat-message__cancel-edit-button" type="reset">Cancel</button>
        </div>
      </form>
    </div>
  );
}

(MessageComponent as any).propTypes = {
  message: propTypes.object.isRequired,
  user: propTypes.object.isRequired
}
