import React from 'react';
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import { formatDate } from '../../helpers/date-helpers';

import './index.css';

export const ChatHeader = (
  {messages, users}: 
  {messages: Message[], users: User[]}
) => {
  const usersCount: number = users.length;
  const messagesCount: number = messages.length;
  let lastMessageSendTime: Date | null = null;
  for (const message of messages) {
    const messageDate: Date = new Date(message.createdAt);
    if (lastMessageSendTime === null || messageDate > lastMessageSendTime) {
      lastMessageSendTime = messageDate;
    }
  }

  return (
    <div className="chat__chat-header">
      <div className="chat-header__general-inf">
        <h2 className="chat-header__name chat-header__general-inf-item">My Chat</h2>
        <div className="chat-header__users-count chat-header__general-inf-item">{usersCount} Participants</div>
        <div className="chat-header__messages-count chat-header__general-inf-item">{messagesCount} messages</div>
      </div>
      <div className="char-header__date">
        { lastMessageSendTime ? 'Last message at ' + formatDate(lastMessageSendTime): '-' }
      </div>
    </div>
  )  
}

(ChatHeader as any).propTypes = {
  messages: propTypes.arrayOf(propTypes.object).isRequired
}
