import React, { useState } from 'react';
import { ChatComponent } from './components/chat'; 
import { LoginComponent } from './components/login';
import { AppHeader } from './components/app-header';
import { AppFooter } from './components/app-footer/';
import { Preloader } from './components/preloader';
import { User } from './types/User';
import { Message } from './types/Message';
import { messageService } from './services/message.service';
import { userService } from './services/users.service';

import './App.css';

function App() {
  const [user, setUser] = useState<User | null>(null);
  const [users, setUsers] = useState<User[]>([]);
  const [messages, setMessages] = useState<Message[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onAuthorized = (user: User) => {
    setUser(user);
    setIsLoading(true);
    messageService.subscribe((messages: Message[]) => { setMessages(messages); setIsLoading(false) });
    userService.subscribe((users: User[]) => setUsers(users));
  };

  const getContent = () => {
    return isLoading ? <Preloader />: <ChatComponent messages={messages} user={user!} users={users} />;
  };

  return (
    <div className="app">
      <AppHeader />
      {user 
        ? getContent()
        : <LoginComponent onAuthorized={onAuthorized} />
      }
      <AppFooter />
    </div>
  );
}

export default App;